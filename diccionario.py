if __name__ == "__main__":
    print("Mi diccionario")
    diccionario = {
        "amigo" : ["friend","Un amigo es una persona con quien se mantiene una amistad. .","A friend is a person with whom a friendship is maintained..\n"],
        "carnivoro" : ["carnivorous"," s un organismo que obtiene sus energías y requerimientos nutricionales a través de una dieta que consiste principal o exclusivamente del consumo de carne,","DIt is an organism that obtains its energies and nutritional requirements through a diet that consists mainly or exclusively of the consumption of meat,"],
        "carro" : ["car","Vehículo formado por una plataforma con barandillas montada sobre ruedas\n","Vehicle formed by a platform with railings mounted on wheels"],
    }

    print(diccionario)
    print("Desea ver el segundo diccionario?(s/n)\n")
    res = input(' ')
    if res.lower() == 's' or res == 'S':
        diccionario.clear()
        diccionario2 = {
            "Saludar": ["Greet",
                        "Es un acto comunicacional en que una persona hace notar a otra su presencia generalmente a través del habla o de algún gesto.",
                        "It is a communicational act in which a person notices another person's presence usually through speech or some gesture."],
            "Computadora": ["Computer", "Es una máquina que está diseñada para facilitarnos la vida.",
                            "It is a machine that is designed to make our lives easier."]
        }
        print(diccionario2)
        print (diccionario2["Computadora"])
    else:
        print("Adios")
